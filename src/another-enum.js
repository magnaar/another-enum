'use strict'

module.exports = {
    Enum: new Proxy({}, new (require('./enum-creator'))),
    EnumValue: require('./enum-value')
}
