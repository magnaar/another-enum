
const { hidden, toString } = require('./symbols')

module.exports = function initEnumValue(thisEnum, enumValue)
{
    enumValue.init(thisEnum)
    if (thisEnum[hidden].values[enumValue.value])
        throwValueAlreadyUsed(thisEnum, +enumValue)
    thisEnum[hidden].values[enumValue.value] = enumValue
    setDebugProperty(enumValue)
}

function setDebugProperty(enumValue)
{
    const toStringDescriptor = {
        enumerable: true,
        writable: false,
        configurable: false
    }
    Object.defineProperty(toStringDescriptor, 'value', {
        get() { return enumValue.toString() }
    })
    Object.defineProperty(enumValue, toString, toStringDescriptor)
    enumValue[hidden].stringValue = undefined
}

function throwValueAlreadyUsed(thisEnum, value)
{
    const enumValue = thisEnum[hidden].values[value]
    const base = isNaN(thisEnum[hidden].base) || thisEnum[hidden].base === 10
        ? ""
        : thisEnum[hidden].base + ":"
    throw Error(`The value "${base}${enumValue.stringValue}" is already used for "${thisEnum.name}.${enumValue}"`)
}
