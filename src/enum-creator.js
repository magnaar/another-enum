'use strict'

const Enum = require('./enum')

module.exports
    = class EnumCreator
    {
        get(target, name)
        {
            if (this[name])
                return this[name]
            return (...args) => new Enum(name, args)
        }

        parse(string)
        {
            let obj = JSON.parse(string)
            let params = []
            const name = Object.keys(obj)[0]
            obj = obj[name]
            if (obj.base)
                params.push(obj.base)
            if (obj.values instanceof Array)
                params = [...params, ...obj.values]
            else
                params = [...params, obj.values]
            return new Enum(name, params)
        }
    }
