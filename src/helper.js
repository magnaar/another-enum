'use strict'

module.exports
    = class Helper
    {
        static parseProperties(obj, parser, visited = [])
        {
            Object.entries(obj)
                .forEach(([prop, value]) => {
                    if (typeof value === 'string')
                        obj[prop] = parser(value) || value
                    else if (Object(value) === value && ! visited.includes(value))
                    {
                        visited.push(value)
                        Helper.parseProperties(value, parser, visited)
                    }
                })
            return obj
        }

        static splitArgs(values, enumValueConstructor)
        {
            const args = {
                options: {
                    base: NaN,
                    class: enumValueConstructor
                },
                values
            }
            if (1 < values.length && typeof values[0] != 'string')
            {
                ({
                    'object': () => args.options = Object.assign(args.options, values[0]),
                    'number': () => args.options.base = values[0],
                    'function': () => args.options.class = values[0]
                })[typeof values[0]]()
                args.values = values.slice(1)

                if (! Helper.inheritsFrom(args.options.class, enumValueConstructor))
                    throw new Error(`"${args.options.class.name}" doesn\'t extend EnumValue`)
            }
            return args
        }

        static inheritsFrom(subclass, inheritsFromClass)
        {
            const inheritsFromPrototype = inheritsFromClass.prototype
            let prototype = subclass.prototype
            while (prototype && prototype !== inheritsFromPrototype)
                prototype = Object.getPrototypeOf(prototype)
            return prototype === inheritsFromPrototype
        }

        static valueToBaseString(base, value, values)
        {
            if (base == 10 || isNaN(base))
                return value.toString()
            else
            {
                const length = Math.max(...Object.keys(values)
                    .map(k => (+k).toString(base).length)
                )
                value = value.toString(base).toUpperCase()
                return "0".repeat(length - value.length) + value
            }
        }
    }
