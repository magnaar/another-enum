'use strict'

class Empty {}

module.exports = {
    enumPropertyNames: [],
    enumValuePropertiesNames: [],
    classPropertiesNames: Object.getOwnPropertyNames(Empty),
 }
 