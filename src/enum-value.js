'use strict'

const { hidden } = require('./symbols')
const { enumValuePropertiesNames } = require('./properties-names')
const { valueToBaseString } = require('./helper')

class EnumValue
{
    constructor(parent, name, value)
    {
        Object.defineProperty(this, hidden, {
            enumerable: false,
            writable: false,
            configurable: false,
            value: {
                name,
                value,
                index: parent[hidden].enumValues.length,
                parent: parent,
                longName: `${parent.name}.${name}`
            }
        })
    }

    init(enumParent)
    {
    }

    get name()
    {
        return this[hidden].name
    }

    get longName()
    {
        return this[hidden].longName
    }

    get value()
    {
        return this[hidden].value
    }

    get stringValue()
    {
        if (! this[hidden].stringValue)
        {
            try
            {
                const base = this[hidden].parent[hidden].base
                const values = this[hidden].parent[hidden].values
                this[hidden].stringValue = valueToBaseString(base, this.value, values)
            }
            catch (error)
            {
                return error
            }
        }
        return this[hidden].stringValue
    }

    get index()
    {
        return this[hidden].index
    }

    isIn(value)
    {
        return this.value && (this.value & value) === this.value || this.value === +value
    }

    toJSON()
    {
        return this.longName
    }

    [Symbol.toPrimitive](hint)
    {
        if (hint == "number")
            return this.value
        return this.name
    }

    get [Symbol.toStringTag]()
    {
        return this.toString()
    }

    toString()
    {
        const base = this[hidden].parent[hidden].base
        let basePrefix = (base == 10 || isNaN(base) ? '' : base + ":")
        return `${this[hidden].longName}(${basePrefix}${this.stringValue})`
    }
}

enumValuePropertiesNames.push(...Object.getOwnPropertyNames(EnumValue.prototype))

module.exports = EnumValue
