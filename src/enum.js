'use strict'

const { enumPropertyNames } = require('./properties-names')
const { hidden } = require('./symbols')
const { parseProperties, splitArgs, valueToBaseString } = require('./helper')
const addEnumValue = require('./add-enum-value')
const addEnumMethods = require('./add-enum-methods')
const initEnumValue = require('./init-enum-value')
const EnumValue = require('./enum-value')

class Enum
{
    constructor(enumName, args)
    {
        args = splitArgs(args, EnumValue)
        Object.defineProperty(this, hidden, {
            enumerable: false,
            writable: false,
            configurable: false,
            value: {
                name: enumName,
                enumValues: [],
                values: {},
                valueConstructor: args.options.class,
                base: args.options.base
            }
        })
        if (typeof args.values[0] == "string")
            args.values.forEach((v, i) => addEnumValue(this, enumName, v, i))
        else
            Object.keys(args.values[0])
                .forEach(v => addEnumValue(this, enumName, v, args.values[0][v]))
        this[hidden].enumValues.forEach((v) => initEnumValue(this, v))
        if (this[hidden].valueConstructor !== EnumValue)
            addEnumMethods(this, this[hidden].valueConstructor)
    }

    get name()
    {
        return this[hidden].name
    }

    get length()
    {
        return this[hidden].enumValues.length
    }

    get(value)
    {
        return this[hidden].values[value]
    }

    getAt(index)
    {
        return this[hidden].enumValues[index]
    }

    hasIn(value, ...maskComponents)
    {
        let mask = 0
        let maskComponent
        for (let i = 0; i < maskComponents.length; ++i)
        {
            maskComponent = maskComponents[i]
            if (typeof maskComponent !== "number")
                maskComponent = this[[maskComponent]]
            if (maskComponent === undefined)
                return false
            mask |= maskComponent
        }
        return (value & mask) === mask
    }

    in(value)
    {
        if (value == 0)
            return this.get(value)
        return this[hidden].enumValues
            .filter(ev => +ev && (ev & value) === +ev)
    }

    parse(toParse)
    {
        if (typeof toParse === 'string')
            return this[hidden].enumValues
                .find(ev => ev.longName === toParse || JSON.stringify(ev) === toParse) || null
        return parseProperties(toParse, str => this.parse(str))
    }

    *[Symbol.iterator]()
    {
        let length = this[hidden].enumValues.length
        for (let i = 0; i < length; ++i)
            yield this[hidden].enumValues[i]
    }

    get [Symbol.toStringTag]()
    {
        return this.toString()
    }

    toString(value)
    {
        if (value === undefined)
            return `${this.name}(${Object.keys(this).join("|")})`
        var values = this.in(value)
        if (values.length == 0)
            return
        const base = this[hidden].base
        const basePrefix = (base == 10 || isNaN(base) ? '' : base + ":")
        const stringValue = valueToBaseString(base, +value, this[hidden].values)
        return `${this.name}[${values.join("|")}](${basePrefix}${stringValue})`
    }

    toJSON()
    {
        const isSimpleValues = this[hidden].enumValues.every(ev => ev.value === ev.index)
        let values = {}
        if (isSimpleValues)
            values = this[hidden].enumValues.map(ev => ev.name)
        else
            this[hidden].enumValues.forEach(ev => (values[ev.name] = ev.stringValue))
        return {
            [this.name]: {
                base: (isNaN(this[hidden].base) ? void 0 : this[hidden].base),
                values
            }
        }
    }
}

enumPropertyNames.push(...Object.getOwnPropertyNames(Enum.prototype))

module.exports = Enum
