'use strict'

const { enumValuePropertiesNames, classPropertiesNames } = require('./properties-names')

module.exports = addEnumMethods

function addEnumMethods(thisEnum, valueConstructor)
{
    const valuePrototype = valueConstructor.prototype
    const ignoredNames = enumValuePropertiesNames
    Object.getOwnPropertyNames(valuePrototype)
        .filter(n => ! ignoredNames.includes(n))
        .forEach(n => addMemberCaller(thisEnum, n, valuePrototype))
    Object.getOwnPropertyNames(valueConstructor)
        .filter(n => ! classPropertiesNames.includes(n))
        .forEach(n => addStaticMemberCaller(thisEnum, n, valueConstructor))
}

function addMemberCaller(thisEnum, name, prototype)
{
    if (thisEnum[name])
        throw new Error(`Can't add the member "${name}" on Enum "${thisEnum.name}", `
            + `the name is already defined`)
    
    const getEnumValue = (value) => (thisEnum[[value]] || thisEnum.get(value))
    const originalDescriptor = Object.getOwnPropertyDescriptor(prototype, name)
    const descriptor = {
        enumerable: false,
        writable: false,
        configurable: false
    }
    descriptor.value = (! originalDescriptor.get && ! originalDescriptor.set
        ? (value, ...args) => originalDescriptor.value.apply(getEnumValue(value), ...args)
        : (value, ...args) => {
                const enumValue = getEnumValue(value)
                return args.length === 0
                    ? enumValue[name]
                    : (enumValue[name] = args[0])
            }
        )
    Object.defineProperty(thisEnum, name, descriptor)
}

function addStaticMemberCaller(thisEnum, name, constructor)
{
    if (thisEnum[name])
        throw new Error(`Can't add the static member "${name}" on Enum "${thisEnum.name}", `
            + `the name is already defined`)
    let descriptor = Object.getOwnPropertyDescriptor(constructor, name)
    if (! descriptor.get && ! descriptor.set)
        descriptor = {
            enumerable: false,
            writable: false,
            configurable: false,
            value: constructor[name]
        }
    Object.defineProperty(thisEnum, name, descriptor)
}
