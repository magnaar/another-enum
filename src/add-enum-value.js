'use strict'

const { hidden } = require('./symbols')
const { enumPropertyNames } = require('./properties-names')
const Helper = require('./helper')

module.exports = addEnumValue

function addEnumValue(thisEnum, enumName, name, value)
{
    if (enumPropertyNames.includes(name))
        throw Error(`${enumName}."${name}" as EnumValue name is forbidden`)
    if (thisEnum[name])
        throw Error(`"${name}" is already defined in "${enumName}"`)
    if (! isNaN(thisEnum[hidden].base) && typeof value == "string")
        value = parseInt(value, thisEnum[hidden].base)
    const result = getConstructorAndSpecialValue(thisEnum, value)
    const constructor = result.constructor
    const newEnumValue = new (constructor)(thisEnum, name, result.value)
    // newEnumValue.init(thisEnum)
    // if (thisEnum[hidden].values[newEnumValue.value])
    //     throwValueAlreadyUsed(thisEnum, enumName, newEnumValue.value)
    // thisEnum[hidden].values[newEnumValue.value] = newEnumValue
    thisEnum[hidden].enumValues.push(thisEnum[name] = newEnumValue)
}

function getConstructorAndSpecialValue(thisEnum, value)
{
    let constructor = thisEnum[hidden].valueConstructor
    if (new Object(value) === value)
    {
        if (value.class)
        {
            if (!Helper.inheritsFrom(value.class, constructor))
                throw new Error(`"${value.class.name}" doesn\'t extend ${constructor.name}`)
            constructor = value.class
        }
        value = typeof value === 'function'
            ? value.call(thisEnum, thisEnum)
            : value.value || getDefaultValue(thisEnum)
    }
    return { constructor, value }
}

function getDefaultValue(thisEnum)
{
    const values = thisEnum[hidden].enumValues
    if (! values.length)
        return 0
    const lastValue = values[values.length - 1]
    return +lastValue + 1
}

// function throwValueAlreadyUsed(thisEnum, enumName, value)
// {
//     const enumValue = thisEnum[hidden].values[value]
//     const base = isNaN(thisEnum[hidden].base) || thisEnum[hidden].base === 10
//         ? ""
//         : thisEnum[hidden].base + ":"
//     throw Error(`The value "${base}${enumValue.stringValue}" is already used for "${enumName}.${enumValue}"`)
// }
