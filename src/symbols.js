'use strict'

module.exports = {
    hidden: Symbol.for("hidden"),
    toString: Symbol()
}
