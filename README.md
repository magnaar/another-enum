
**ANOTHER-ENUM** [![changelog](http://magnaar.io/badges/another-enum/light-version)](https://gitlab.com/magnaar/another-enum-doc/blob/master/docs/changelog.md)
=================
### _Another enum module, nothing more_

---
[![npm install](http://magnaar.io/badges/another-enum/npm-install)]()

[![docs](http://magnaar.io/badges/another-enum/docs)]()
[![build](http://magnaar.io/badges/another-enum/build)]()
[![tests](http://magnaar.io/badges/another-enum/tests)]()
[![coverage](http://magnaar.io/badges/another-enum/coverage)]()
[![audit](http://magnaar.io/badges/another-enum/audit)]()
[![dependencies](http://magnaar.io/badges/another-enum/dependencies)]()


---

## Simple usage
- [Import](#import)
- [Basic instanciation](#basic-instanciation)

## Advanced usage
- [Instanciation with base](https://gitlab.com/magnaar/another-enum-doc/blob/master/docs/base-instanciation.md)
- [Custom methods](https://gitlab.com/magnaar/another-enum-doc/blob/master/docs/custom-methods.md)
- [Serializing](https://gitlab.com/magnaar/another-enum-doc/blob/master/docs/serializing.md)
- [Bitmask](https://gitlab.com/magnaar/another-enum-doc/blob/master/docs/bitmask.md)
- [**`Special cases`**](https://gitlab.com/magnaar/another-enum-doc/blob/master/docs/special-cases.md)

## Methods listing
- [Enum Constructors](https://gitlab.com/magnaar/another-enum-doc/blob/master/docs/enum-constructors.md)
- [Enum Methods](https://gitlab.com/magnaar/another-enum-doc/blob/master/docs/enum.md)
- [EnumValue Methods](https://gitlab.com/magnaar/another-enum-doc/blob/master/docs/enum-value.md)

### **Import**
```
import { Enum } from 'another-enum'
// Or
const Enum = require('another-enum').Enum
```

### **Basic instanciation**
```
Enum.EnumName(NAME1, NAME2, ...)

Enum.EnumName({
   NAME1: value1,
   NAME2: value2,
   ...
})
```
See also: [Enum Constructors](https://gitlab.com/magnaar/another-enum-doc/blob/master/docs/enum-constructors.md)

See also: [Instanciation with base](https://gitlab.com/magnaar/another-enum-doc/blob/master/docs/base-instanciation.md)

See also: [Custom methods](https://gitlab.com/magnaar/another-enum-doc/blob/master/docs/custom-method.md)

#### _Examples_
```
const Colors = Enum.Colors('RED', 'GREEN', 'BLUE')

const HexaColors = Enum.HexaColors(16, {
    RED: 0xFF0000,
    GREEN: 0x00FF00,
    BLUE: 0x0000FF
})
// For more explanation, you should take a look at:
// - Enum constructors
// - Instanciation with base
```
```
// Some nice constructors
// Automatic incrementation
Enum.Values({
    VALUE1: 2,
    VALUE2: {}, // value: 3
    VALUE3: 8,
    VALUE4: {}, // value: 9
    VALUE5: {}  // value: 10
})
```

```
const obj = {
    [Values.VALUE1]: 'value1',
    [Values.VALUE2]: 'value2'
}
```
```
switch (color)
{
    case Values.VALUE1:
        // ...
        break
    case Values.VALUE2:
        // ...
        break
    default:
        // ...
}
```

---
[![version](http://magnaar.io/badges/another-enum/version)](http://www.npmjs.com/package/another-enum) [![npm install](http://magnaar.io/badges/another-enum/npm-install)](https://www.npmjs.com/package/another-enum)